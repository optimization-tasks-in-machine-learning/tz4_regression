# ТЗ4 Регрессия

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz4_regressia.pdf](illustrations/tz4_regressia.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1xFtUwFpf32WilNGaL5CB0LKVE1OrBWwA?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz4_regressia.ipynb](tz4_regressia.ipynb)

## Иллюстрация работы

* Линейная регрессия
![Solution of problem](illustrations/linear_regression.png) <br/>
* Полиномиальная регрессия
![Interactive visualization](illustrations/polynomial_regression.png) <br/>
* Экспоненциальная регрессия
![Interactive visualization](illustrations/exponencial_regression.png) <br/>
* L1 регуляция
![Interactive visualization](illustrations/L1_regularization.png) <br/>
* L2 регуляция
![Interactive visualization](illustrations/L2_regularization.png) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz4_regressia.git
# Импорт всех функций из модуля
from tz4_regressia.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module module tz4_regressia.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz4_regressia.utils:

visualize_plotly(X, y, input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
    Визуализация полученной функции с предикторами. 
    
    Parameters
    ===========
    
    X: list, numpy.ndarray
        Массив предикторов
    y: list, numpy.ndarray
    input_investigated_function: str
        Исследуемая функция
    functions_symbols: tuple, optional
        Переменные функции
        Массив предсказываемой переменной
    is_display_input: bool
        Отображать входную функцию или нет
    start: int, float
        Начало графика
    stop: int, float
        Конец графика
    detail: int, float
        Детализация графика
```

`help(find_extremums)`

```
Help on function multidimensional_optimization in module tz4_regressia.utils:

regression(X: list, y: list, regression_method: Union[int, str], polynomial_degree: Union[int, NoneType] = None, regularization_method: Union[int, str, NoneType] = None, L_degree: float = 100.0, is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Регрессия с заданными параметрами. 
    
    Parameters
    ===========
    
    X: list, numpy.ndarray
        Массив предикторов
    y: list, numpy.ndarray
        Массив предсказываемой переменной
    regression_method: int, str
        Методы регрессии
        0, 'linear': линейная
        1, 'polynomial': полиномиальная
        2, 'exponencial': экспоненциальная
    polynomial_degree: int, oprional
        Степень полиномиальной регрессии
    regularization_method: int, str, optional
        Методы регуляризации
        0, L1
        1, L2
    L_degree: float
        Степень L-регуляризации
    is_display_input: bool = False
        Отображать входные данные или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет
    is_try_visualize: bool = False
        Визуализация процесса
    
    Returns
    ===========
    
    function_result: str
        Получившееся функция
    coefficients: list
        Массив коэффициентов
    free_term: float
        Свободный член
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)
